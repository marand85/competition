package com.mariusz_andrzejewski.competition.atmservice.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @EqualsAndHashCode
public class ATM {

    private int region;
    private int atmId;
    
}