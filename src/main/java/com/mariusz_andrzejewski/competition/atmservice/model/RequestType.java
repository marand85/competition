package com.mariusz_andrzejewski.competition.atmservice.model;

public enum RequestType {
    STANDARD,
    SIGNAL_LOW,
    PRIORITY,
    FAILURE_RESTART
}