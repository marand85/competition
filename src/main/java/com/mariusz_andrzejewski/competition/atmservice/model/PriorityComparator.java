package com.mariusz_andrzejewski.competition.atmservice.model;

import java.util.Comparator;

public class PriorityComparator implements Comparator<Task> {

    public int compare(Task t1, Task t2) {
        return t2.getRequestType().compareTo(t1.getRequestType());
    }
}