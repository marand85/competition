package com.mariusz_andrzejewski.competition.atmservice.model;

import java.util.Comparator;

public class RegionComparator implements Comparator<Task> {

    public int compare(Task t1, Task t2) {
        return t1.getRegion() - t2.getRegion();
    }
}