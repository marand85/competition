package com.mariusz_andrzejewski.competition.atmservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Task {

    private int region;
    private RequestType requestType;
    private int atmId;
    
}