package com.mariusz_andrzejewski.competition.atmservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mariusz_andrzejewski.competition.atmservice.model.ATM;
import com.mariusz_andrzejewski.competition.atmservice.model.Task;
import com.mariusz_andrzejewski.competition.atmservice.service.ATMService;

@RestController
@RequestMapping("/atms")
public class ATMController {
    
    private ATMService atmService;

    @Autowired
    public ATMController(ATMService atmService) {
        this.atmService = atmService;
    }

    @PostMapping("/calculateOrder")
    public ResponseEntity<List<ATM>> calculateOrder(@RequestBody List<Task> serviceTasks) {

        List<ATM> atmsOrder = atmService.calculateAtmsOrder(serviceTasks);

        return ResponseEntity.ok(atmsOrder);
        
    }
}