package com.mariusz_andrzejewski.competition.atmservice.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.mariusz_andrzejewski.competition.atmservice.model.ATM;
import com.mariusz_andrzejewski.competition.atmservice.model.RegionComparator;
import com.mariusz_andrzejewski.competition.atmservice.model.PriorityComparator;
import com.mariusz_andrzejewski.competition.atmservice.model.Task;

@Service
public class ATMService {

    public List<ATM> calculateAtmsOrder(List<Task> serviceTasks) {

        Comparator<Task> regionComparator = new RegionComparator();
        Comparator<Task> priorityComparator = new PriorityComparator();
        Comparator<Task> taskComparator = regionComparator.thenComparing(priorityComparator);

        Queue<Task> pq = new PriorityQueue<>(taskComparator);
        List<ATM> atmsList = new ArrayList<>();

        pq.addAll(serviceTasks);
        Task task;
        while (pq.iterator().hasNext()) {            
            task = pq.poll();
            atmsList.add(new ATM(task.getRegion(), task.getAtmId()));
        }

        // removing duplicates
        Set<ATM> atmsSet = new LinkedHashSet<>();
        atmsSet.addAll(atmsList);
        atmsList.clear();
        atmsList.addAll(atmsSet);

        return atmsList;            
        
    }
}