package com.mariusz_andrzejewski.competition.onlinegame.model;

import java.util.Comparator;

public class ClansByNrOfPointsComparator implements Comparator<Clan> {

    public int compare(Clan clan1, Clan clan2) {
        return clan1.getPoints() - clan2.getPoints();
    }

}