package com.mariusz_andrzejewski.competition.onlinegame.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Clan {

    private int numberOfPlayers;
    private int points;

}


