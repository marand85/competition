package com.mariusz_andrzejewski.competition.onlinegame.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Players {

    private int groupCount;
    private List<Clan> clans;

}