package com.mariusz_andrzejewski.competition.onlinegame.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mariusz_andrzejewski.competition.onlinegame.model.Clan;
import com.mariusz_andrzejewski.competition.onlinegame.model.Players;
import com.mariusz_andrzejewski.competition.onlinegame.service.OnlinegameService;

@RestController
@RequestMapping("/onlinegame")
public class OnlinegameController {
    
    private OnlinegameService onlinegameService;

    @Autowired
    public OnlinegameController(OnlinegameService onlinegameService) {
        this.onlinegameService = onlinegameService;
    }

    @PostMapping("/calculate")
    public ResponseEntity<List<List<Clan>>> calculateOrder(@RequestBody Players groupCountAndPlayersList) {

        List<List<Clan>> orderdGroupsOfClans = onlinegameService.orderedListOfGroups(groupCountAndPlayersList);

        return ResponseEntity.ok(orderdGroupsOfClans);
        
    }
}