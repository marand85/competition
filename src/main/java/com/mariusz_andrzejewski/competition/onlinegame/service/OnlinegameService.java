package com.mariusz_andrzejewski.competition.onlinegame.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.mariusz_andrzejewski.competition.onlinegame.model.Clan;
import com.mariusz_andrzejewski.competition.onlinegame.model.Players;
import com.mariusz_andrzejewski.competition.onlinegame.model.ClansByNrOfPlayersComparator;
import com.mariusz_andrzejewski.competition.onlinegame.model.ClansByNrOfPointsComparator;

@Service
public class OnlinegameService {

    public List<List<Clan>> orderedListOfGroups(Players groupSizeAndPlayersList) {

        // Empty = place for output data
        List<List<Clan>> orderedGroupsOfClans = new ArrayList<List<Clan>>();
        // How many can be in a group
        int groupPlayersCountLimit = groupSizeAndPlayersList.getGroupCount();
        // Clans from HTTP request
        List<Clan> clansList = groupSizeAndPlayersList.getClans();
        // Comparator orders Clans by points ascending (the first = the least points)
        Comparator<Clan> clansByNrOfPointsComparator = new ClansByNrOfPointsComparator();
        // Comparator orders Clans by number of players ascending
        Comparator<Clan> clansByNrOfPlayersComparator = new ClansByNrOfPlayersComparator();
        // a single group within the output
        List<Clan> groupClans = new ArrayList<>();

        Queue<Clan> pqClans = new PriorityQueue<>(clansByNrOfPointsComparator.reversed().thenComparing(clansByNrOfPlayersComparator));
        Queue<Clan> pqFilteredClans = new PriorityQueue<>((clansByNrOfPlayersComparator.reversed()).thenComparing(clansByNrOfPointsComparator.reversed()));
        boolean pqFilteredClansIsNotEmpty = true; // default not empty

        pqClans.addAll(clansList);

        while (!pqClans.isEmpty()) { // all players must get to game server            
            Clan clan = pqClans.poll(); // first clan with the most points always gets first to the group
            groupClans.add(clan);
            int clansGroupPlayersActualCount = clan.getNumberOfPlayers();
            while(true) {
                Integer cGPAC = clansGroupPlayersActualCount; // workaround - local variable defined in an enclosing scope must be final or effectively final
                pqFilteredClans.clear();
                List<Clan> filteredClansList = pqClans.stream()
                    .filter(c->(c.getNumberOfPlayers() + cGPAC.intValue() <= groupPlayersCountLimit)) // groupPlayersCountLimit is effectively final so no error is thrown
                    .collect(Collectors.toCollection(ArrayList<Clan>::new));
                pqFilteredClans.addAll(filteredClansList);
                pqFilteredClansIsNotEmpty = !pqFilteredClans.isEmpty();
                if (pqFilteredClansIsNotEmpty) {
                    clan = pqFilteredClans.poll();
                    groupClans.add(clan);
                    clansGroupPlayersActualCount += clan.getNumberOfPlayers();
                    pqClans.remove(clan);
                } else {
                    // This will not work!!!: orderedGroupsOfClans.add(groupClans);
                    // Add a copy of an object, not reference to it, because later you clear that object so the reference will point to null
                    orderedGroupsOfClans.add(new ArrayList<Clan>(groupClans));
                    groupClans.clear();
                    clansGroupPlayersActualCount = 0;
                    break;
                }
            }
        }
        return orderedGroupsOfClans;
    }
}
