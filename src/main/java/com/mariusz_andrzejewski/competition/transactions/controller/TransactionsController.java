package com.mariusz_andrzejewski.competition.transactions.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mariusz_andrzejewski.competition.transactions.model.Account;
import com.mariusz_andrzejewski.competition.transactions.model.Transaction;
import com.mariusz_andrzejewski.competition.transactions.service.TransactionsService;

@RestController
@RequestMapping("/transactions")
public class TransactionsController {
    
    private TransactionsService transactionsService;

    @Autowired
    public TransactionsController(TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }

    @PostMapping("/report")
    public ResponseEntity<List<Account>> processTransactions(@RequestBody List<Transaction> transactionsList) {

        List<Account> sortedAccounts = transactionsService.executeReport(transactionsList);

        return ResponseEntity.ok(sortedAccounts);
        
    }
}