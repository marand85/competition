package com.mariusz_andrzejewski.competition.transactions.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class Transaction {

    private String debitAccount;
    private String creditAccount;
    private String amount;
    
}