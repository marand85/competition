package com.mariusz_andrzejewski.competition.transactions.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @EqualsAndHashCode
public class Account {

    private String account;
    private int debitCount;
    private int creditCount;
    private String balance;

}