package com.mariusz_andrzejewski.competition.transactions.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import com.mariusz_andrzejewski.competition.transactions.model.Account;
import com.mariusz_andrzejewski.competition.transactions.model.Transaction;


@Service
public class TransactionsService {

    public List<Account> executeReport(List<Transaction> transactionsList) {

        List<Account> accountsList = new ArrayList<>();
        Set<String> accountsNumbersSet = new TreeSet<>();
        Map<String, Account> accountsMap = new LinkedHashMap<>();

        // to do...
        for (Transaction transaction : transactionsList) {

            String accountNumber = transaction.getDebitAccount();
            accountsNumbersSet.add(accountNumber);
            accountNumber = transaction.getCreditAccount();
            accountsNumbersSet.add(accountNumber);
        }
        // now accountsNumbersSet contains unique accounts numbers
        accountsNumbersSet.forEach(accnum -> accountsMap.put(accnum, new Account(accnum, 0, 0, "0")));
        // now I have an ordered map with unique keys and all unique accounts that occurred in values

        // I traverse ArrayList second time to parse transactions
        for (Transaction transaction : transactionsList) {
            Account account;
            // debit accounts
            String accountNumber = transaction.getDebitAccount();
            account = accountsMap.get(accountNumber);
            account.setDebitCount(account.getDebitCount()+1);
            BigDecimal bddb = new BigDecimal(account.getBalance()); // bddb states for big decimal debit balance
            BigDecimal bdsub = bddb.subtract(new BigDecimal(transaction.getAmount())); // bdsub states for big decimal subtract
            account.setBalance(bdsub.toString());
            accountsMap.put(accountNumber, account);
            // credit accounts
            accountNumber = transaction.getCreditAccount();
            account = accountsMap.get(accountNumber);
            account.setCreditCount(account.getCreditCount()+1);
            BigDecimal bdcb = new BigDecimal(account.getBalance()); // bdcb states for big decimal credit balance
            BigDecimal bdadd = bdcb.add(new BigDecimal(transaction.getAmount())); // bdadd states for big decimal add
            account.setBalance(bdadd.toString());
            accountsMap.put(accountNumber, account);
        }

        // I make a list from the map
        accountsList = new ArrayList<Account>(accountsMap.values());

        return accountsList;
                
    }
}